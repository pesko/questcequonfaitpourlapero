class PagesController < ApplicationController
    def index
        time = Time.new
        if time.min > 10
            now = "#{time.hour}h#{time.min}"
        else
            now = "#{time.hour}h0#{time.min}"
        end

        if time.hour < 12
            @text = "Apéro à #{now} ? Vous êtes chaud !"
        elsif time.hour >= 12 && time.hour < 17
            @text = "L'apéro de #{now}, c'est le meilleur... Bon après-midi !"
        elsif time.hour >= 17 && time.hour < 23
            @text = "#{now}, l'heure parfaite pour l'apéro !"
        elsif time.hour >= 23 && time.hour < 8
            @text = "Je ne sais pas si on peut toujours appeler ça un Apéro à #{now} !"
        end
    end
end
