class PreferedsController < ApplicationController
    
    def index
        questions = Prefered.all
        @question = questions.sample 
        @vote1 = @question.vote1 + 1
        @vote2 = @question.vote2 + 1    

        pourcentage_vote_1
        pourcentage_vote_2
    end
    
    def show
        @question = Prefered.find(params[:id])
        vote1 = @question.vote1 + 1
        
        if @question.update(vote1: vote1)
            redirect_to prefereds_path
        end
    end

    def new
        @question = Prefered.new
    end

    def create
        @question = Prefered.create(question_params)
        redirect_to prefereds_path
    end

    
    def update_vote2
        @question = Prefered.find(params[:id])
        vote2 = @question.vote2 + 1
        if @question.update(vote2: vote2)
            redirect_to prefereds_path
        end
    end
    
    private 

    def question_params
        params.require(:prefered).permit(:text, :text2)
    end
    
    def pourcentage_vote_1
        total1 = @vote1 + @question.vote2
        @pourcent1 = (@vote1 * 100) / total1
    end
    
    def pourcentage_vote_2
        total2 = @vote2 + @question.vote1
        @pourcent2 = (@vote2 * 100) / total2
    end
end
