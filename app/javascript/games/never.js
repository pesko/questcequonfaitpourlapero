const wrapperNever = document.querySelector('.wrapper-never')

if (wrapperNever){
    const questionsNever = fetch('https://questcequonfaitpourlapero.herokuapp.com/api/v1/nevers');
    // const questionsNever = fetch('http://localhost:3000/api/v1/nevers');


    questionsNever
        .then(data => data.json())
        .then(data => { randomQuestion(data) })
        .catch( (err) => { console.log(err) })

        function randomQuestion(data) {
            const insertQuestion = document.querySelector('.question')
            const question = data[Math.floor(Math.random() * data.length)];
            const element = `
                <p>${question.text}</p>
            `
            insertQuestion.innerHTML = element;

            const next = document.querySelector('.next button')
            next.addEventListener('click', (e) => {
                const insertQuestion = document.querySelector('.question')
                const question = data[Math.floor(Math.random() * data.length)];
                const element = `
                    <p>${question.text}</p>
                `
                insertQuestion.innerHTML = element;
            })
        }
}