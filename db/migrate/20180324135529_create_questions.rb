class CreateQuestions < ActiveRecord::Migration[5.1]
  def change
    create_table :questions do |t|
      t.string :never
      t.string :prefered

      t.timestamps
    end
  end
end
